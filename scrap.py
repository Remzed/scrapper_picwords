# -*- coding: utf-8 -*-
#!/usr/bin/env python

import subprocess, sqlite3
for x in range(1, 600):
 url = 'http://games-answers.info/data/picwords/images/pic_%i.jpg' %x
 subprocess.check_output('curl -s %s > ./pics/%s.jpg' % (url, x), shell=True)

sqlite_file = '/Users/rem/hardwork/playground/scraper_picwords/my_db.sqlite'

table_name = 'scrapper'
id_field = 'id'
w0_field = 'w0'
w1_field = 'w1'
w2_field = 'w2'

conn = sqlite3.connect(sqlite_file)
c = conn.cursor()

c.execute('CREATE TABLE {tn} ({nf} INTERGER PRIMARY KEY)'\
  .format(tn=table_name, nf=id_field))

c.execute('ALTER TABLE {tn} ADD COLUMN {w0} TEXT'\
  .format(tn=table_name, w0=w0_field))

c.execute('ALTER TABLE {tn} ADD COLUMN {w1} TEXT'\
  .format(tn=table_name, w1=w1_field))

c.execute('ALTER TABLE {tn} ADD COLUMN {w2} TEXT'\
  .format(tn=table_name, w2=w2_field))


for x in range(1, 600):
  url = 'http://games-answers.info/fr/picwords/id-%i.html' %x
  payload = subprocess.check_output('curl -s %s|grep \'<title>\'' % (url), shell=True)
  for regex in ['PicWords','<title>','</title>', '\'']:
    if regex in payload:
      payload=payload.replace(regex,'')
  words = payload.split(",")
  w0 = words[0].translate(None, '\t\n ')
  w1 = words[1].translate(None, '\t\n ')
  w2 = words[2].translate(None, '\t\n ')
  print x, w0, w1, w2

  try:
    c.execute("INSERT INTO {tn} ({idf}, {w0_field}, {w1_field}, {w2_field}) VALUES ({x}, '{w0}', '{w1}', '{w2}')".\
        format(tn=table_name, idf=id_field, w0_field=w0_field, w1_field=w1_field, w2_field=w2_field, x=x, w0=w0, w1=w1, w2=w2))
  except sqlite3.IntegrityError:
    print('ERROR: ID already exists in PRIMARY KEY column {}'.format(id_field))


conn.commit()
conn.close()

